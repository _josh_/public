package com.ingima.jllanos.meteo.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Locale;

/**
 * Handle settings of application (language from Android, temperature unit)
 */
public class SettingsHelper {
    public static final String PREF_TEMPERATURE = "temperature_list";
    private static final String PREF_TEMPERATURE_DEFAULT = "metric";

    public static final String UNIT_METRIC_NAME       = "metric";
    public static final String UNIT_FAHRENHEITH_NAME  = "imperial";
    private static final String UNIT_METRIC_VALUE = "°C";
    private static final String UNIT_FAHRENHEITH_VALUE = "°F";
    private static final String UNIT_KELVIN_VALUE = "°K";

    private String unitsText = "°C";

    private static SettingsHelper instance;
    private final SharedPreferences sharedPreferences;

    public static SettingsHelper getInstance(Context context) {
        if (instance == null) {
            instance = new SettingsHelper(context);
        }
        return instance;
    }

    public void resume() {
        MeteoDataSource meteoDataSource = MeteoDataSource.getInstance();
        // Set the language used by phone
        meteoDataSource.setLangText(Locale.getDefault().getLanguage());
        // Set the temparature unit used by phone
        meteoDataSource.setUnitsTemp(getTemperatureUnit());
        setUnitsTemp(getTemperatureUnit());
    }

    public String getUnitsText() {
        return unitsText;
    }

    private SettingsHelper(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    private String getTemperatureUnit() {
        return sharedPreferences.getString(PREF_TEMPERATURE, PREF_TEMPERATURE_DEFAULT);
    }

    public void setUnitsTemp(String unitsTemp) {
        switch (unitsTemp) {
            case UNIT_METRIC_NAME:
                unitsText = UNIT_METRIC_VALUE;
                break;
            case UNIT_FAHRENHEITH_NAME:
                unitsText = UNIT_FAHRENHEITH_VALUE;
                break;
            default:
                unitsText = UNIT_KELVIN_VALUE;
        }
    }
}
