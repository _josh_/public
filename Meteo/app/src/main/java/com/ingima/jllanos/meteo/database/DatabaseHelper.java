package com.ingima.jllanos.meteo.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.ingima.jllanos.meteo.model.ItemCity;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Helper of DataBase use OrmLite to acces base
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {


    private static final String     DB_NAME = "MeteoDatabase.sqlite";
    private static final int        DB_VERSION = 1;

    private Dao<ItemCity, Integer>  cityDAO= null;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, com.j256.ormlite.support.ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, ItemCity.class);
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, com.j256.ormlite.support.ConnectionSource connectionSource, int i, int i2) {
        try {
            TableUtils.dropTable(connectionSource, ItemCity.class, true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /** Getter/Setter
     *
     */
    public Dao<ItemCity, Integer> getCityDao() {
        if (cityDAO == null) {
            try {
                cityDAO = getDao(ItemCity.class);
            }catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return cityDAO;
    }

}