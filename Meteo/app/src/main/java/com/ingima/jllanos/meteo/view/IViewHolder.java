package com.ingima.jllanos.meteo.view;

import com.ingima.jllanos.meteo.model.ItemCity;

/**
 * ViewHolder interface
 */

public interface IViewHolder {
    void bind(ItemCity item);
}
