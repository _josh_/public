package com.ingima.jllanos.meteo.model;


import android.location.Location;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ingima.jllanos.meteo.utils.MeteoDataSource;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;
import java.util.Observable;

/**
 * Model for one City, it's observable
 * several fields are persistent with ormlite
 */
@DatabaseTable(tableName = "cities")
public class ItemCity extends Observable {


    final static String PROVIDER_NAME = "openweathermap";
    final static String JSON_ATTR_NAME    = "name";
    final static String JSON_ATTR_ID      = "id";

    final static String JSON_ATTR_SYS     = "sys";
    final static String JSON_ATTR_SYS_COUNTRY = "country";
    final static String JSON_ATTR_SYS_SUNRISE = "sunrise";
    final static String JSON_ATTR_SYS_SUNSET  = "sunset";

    final static String JSON_ATTR_COORD   = "coord";
    final static String JSON_ATTR_COORD_LAT   = "lat";
    final static String JSON_ATTR_COORD_LON   = "lon";

    final static String JSON_ATTR_WEATHER       = "weather";
    final static String JSON_ATTR_WEATHER_MAIN  = "main";
    final static String JSON_ATTR_WEATHER_DESC  = "description";
    final static String JSON_ATTR_WEATHER_ICON  = "icon";

    final static String JSON_ATTR_DATA           = "main";
    final static String JSON_ATTR_DATA_TEMP      = "temp";
    final static String JSON_ATTR_DATA_TEMP_MIN  = "temp_min";
    final static String JSON_ATTR_DATA_TEMP_MAX  = "temp_max";
    final static String JSON_ATTR_DATA_PRESSURE  = "pressure";
    final static String JSON_ATTR_DATA_HUMIDITY  = "humidity";

    @DatabaseField(generatedId = true)
    private long id;

    @DatabaseField(columnName = "name", canBeNull = false)
    protected String name;
    @DatabaseField(columnName = "country", canBeNull = false)
    protected String country;
    @DatabaseField(columnName = "cityid", canBeNull = false)
    protected String cityId;
    @DatabaseField(columnName = "position", canBeNull = false)
    private int position;


    protected Location location;
    private Date sunrise;
    private Date sunset;
    private String weatherTitle;
    private String weatherDesc;
    private String weatherIcon;
    private Double temp;
    private Double tempMin;
    private Double tempMax;
    private Double pressure;
    private Double humidity;

    public ItemCity() {}

    /**
     * Constructor from basic field (from dataBase)
     */
    public ItemCity(String title, String cityId, String country, int pos) {
        this.name    = title;
        this.cityId  = cityId;
        this.country = country;
        this.position= pos;
    }

    /**
     * Constructor from json data response
     */
    public ItemCity(JsonObject jsonObject) {
        // get default city data
        name     = jsonObject.get(JSON_ATTR_NAME).getAsString();
        cityId   = jsonObject.get(JSON_ATTR_ID).getAsString();
        JsonObject sys = (JsonObject) jsonObject.get(JSON_ATTR_SYS);
        if (sys != null) {
            country = sys.get(JSON_ATTR_SYS_COUNTRY).getAsString();
            try {
                sunrise = new Date(sys.get(JSON_ATTR_SYS_SUNRISE).getAsLong() * 1000l);
                sunset  = new Date(sys.get(JSON_ATTR_SYS_SUNSET).getAsLong() * 1000l);
            } catch (Exception e) {
            }
        }

        // get location data
        try {
            location = new Location(PROVIDER_NAME);
            JsonObject coord = (JsonObject) jsonObject.get(JSON_ATTR_COORD);
            if (coord != null) {
                location.setLatitude(coord.get(JSON_ATTR_COORD_LAT).getAsDouble());
                location.setLongitude(coord.get(JSON_ATTR_COORD_LON).getAsDouble());
            }
        } catch (Exception e) {
        }

        try {
            // get weather data
            JsonArray weatherArr = (JsonArray) jsonObject.get(JSON_ATTR_WEATHER);
            if (weatherArr != null && weatherArr.size() > 0) {
                JsonObject weatherObj = (JsonObject) weatherArr.get(0);
                weatherTitle = weatherObj.get(JSON_ATTR_WEATHER_MAIN).getAsString();
                weatherDesc  = weatherObj.get(JSON_ATTR_WEATHER_DESC).getAsString();
                weatherIcon  = MeteoDataSource.URL_ICONS + weatherObj.get(JSON_ATTR_WEATHER_ICON).getAsString() + MeteoDataSource.URL_ICONS_EXT;
            }
        } catch (Exception e) {
        }

        try {
            JsonObject data = (JsonObject) jsonObject.get(JSON_ATTR_DATA);
            if (data != null) {
                temp = data.get(JSON_ATTR_DATA_TEMP).getAsDouble();
                tempMin = data.get(JSON_ATTR_DATA_TEMP_MIN).getAsDouble();
                tempMax = data.get(JSON_ATTR_DATA_TEMP_MAX).getAsDouble();
                pressure  = data.get(JSON_ATTR_DATA_PRESSURE).getAsDouble();
                humidity  = data.get(JSON_ATTR_DATA_HUMIDITY).getAsDouble();
            }
        } catch (Exception e) {
        }
    }


    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }
    public long getId() {
        return id;
    }

    public String getCityId() {
        return cityId;
    }

    public Location getLocation() {
        return location;
    }
    public String getWeatherTitle() {
        return weatherTitle;
    }

    public String getWeatherDesc() {
        return weatherDesc;
    }

    public String getWeatherIcon() {
        return weatherIcon;
    }

    public Double getTemp() {
        return temp;
    }

    public Double getTempMin() {
        return tempMin;
    }

    public Double getTempMax() {
        return tempMax;
    }

    public Double getPressure() {
        return pressure;
    }

    public Double getHumidity() {
        return humidity;
    }

    public Date getSunrise() {
        return sunrise;
    }

    public Date getSunset() {
        return sunset;
    }


    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    /**
     * Update the contain of one city with value provided by the update Item
     * @param updatedeItemCity
     */
    public void update(ItemCity updatedeItemCity) {
        location     = updatedeItemCity.getLocation();
        sunset       = updatedeItemCity.getSunset();
        sunrise      = updatedeItemCity.getSunrise();
        weatherTitle = updatedeItemCity.getWeatherTitle();
        weatherDesc  = updatedeItemCity.getWeatherDesc();
        weatherIcon  = updatedeItemCity.getWeatherIcon();
        temp         = updatedeItemCity.getTemp();
        tempMin      = updatedeItemCity.getTempMin();
        tempMax      = updatedeItemCity.getTempMax();
        pressure     = updatedeItemCity.getPressure();
        humidity     = updatedeItemCity.getHumidity();

        // Notify model was updated
        setChanged();
        notifyObservers();
    }

    @Override
    public boolean equals(Object obj) {
        // We will use 'city id' to identify each city
        if (obj instanceof ItemCity && cityId != null && cityId.equals(((ItemCity) obj).cityId)) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return cityId.hashCode();
    }
}
