package com.ingima.jllanos.meteo.view;

import android.content.Context;
import android.view.View;

import com.ingima.jllanos.meteo.R;
import com.ingima.jllanos.meteo.model.ItemCity;

import java.util.List;


public class ItemAdapter extends AbstractItemAdapter<ItemViewHolder> {

    public ItemAdapter(List<ItemCity> cityList) {
        super(cityList);
    }

    @Override
    public ItemViewHolder getNewViewHolder(Context context, View view) {
        return new ItemViewHolder(context, view);
    }


    @Override
    public int getLayoutRessource() {
        return R.layout.list_item_city;
    }
}
