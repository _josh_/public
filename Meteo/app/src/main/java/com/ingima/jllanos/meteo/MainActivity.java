package com.ingima.jllanos.meteo;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.ingima.jllanos.meteo.addcity.AddCityActivity;
import com.ingima.jllanos.meteo.database.DBManager;
import com.ingima.jllanos.meteo.model.CurrentLocation;
import com.ingima.jllanos.meteo.model.ItemCity;
import com.ingima.jllanos.meteo.reordercity.ReorderCityActivity;
import com.ingima.jllanos.meteo.settings.SettingsActivity;
import com.ingima.jllanos.meteo.utils.CityManager;
import com.ingima.jllanos.meteo.utils.GpsManager;
import com.ingima.jllanos.meteo.utils.MeteoDataSource;
import com.ingima.jllanos.meteo.utils.SettingsHelper;
import com.ingima.jllanos.meteo.view.ItemAdapter;

import java.util.ArrayList;
import java.util.List;


/**
 * Main Screen
 */
public class MainActivity extends AppCompatActivity {
    private RecyclerView listView;
    RecyclerView.LayoutManager layoutManager;
    private List<ItemCity> listCities = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Init the DBManager
        DBManager.init(this);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        GpsManager.getInstance().init(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Init/Update settings
        SettingsHelper.getInstance(this).resume();

        // load/reload (with update) the list of cities
        loadList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == R.id.action_add_city) {
            // Action to display screen to add city
            if (CityManager.getInstance().getCityList().size() < 20) {
                Intent intent = new Intent(getApplicationContext(), AddCityActivity.class);
                startActivity(intent);
            } else {
                Toast toast = Toast.makeText(this, getString(R.string.error_fail_add_max_cities), Toast.LENGTH_SHORT);
                toast.show();
            }
            return true;
        } else if (id == R.id.action_reorder_city) {
            // Action to display screen to reorder/remove city
            Intent intent = new Intent(getApplicationContext(), ReorderCityActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_preference) {
            // Action to display preference screen
            Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    /**
     * Load and update the list of cities, adding the Last GPS position City on top
     * If no item is available, display the screen to add cities.
     */
    private void loadList() {
        ItemCity currentCity = null;

        listView = (RecyclerView) findViewById(R.id.list_item);
        listCities.clear();
        listCities.addAll(CityManager.getInstance().getCityList());

        Location location = GpsManager.getInstance().getLastKnownLocation();
        if (location != null) {
            currentCity = new CurrentLocation(getString(R.string.current_position_title), location);
            listCities.add(0, currentCity);
        }

        if (listCities.isEmpty() && currentCity == null) {
            // No City to display, display the screen to add cities
            Intent intent = new Intent(this, AddCityActivity.class);
            startActivity(intent);
        } else {
            // Start request to update Data
            MeteoDataSource.getInstance().update(this, listCities);
            if (currentCity != null) {
                MeteoDataSource.getInstance().updateFromLocalisation(this, currentCity);
            }

            // use a linear layout manager
            layoutManager = new LinearLayoutManager(this);
            listView.setLayoutManager(layoutManager);

            // Set the list to display
            RecyclerView.Adapter adapter = new ItemAdapter(listCities);
            listView.setHasFixedSize(true);
            listView.setAdapter(adapter);
        }
    }
}
