package com.ingima.jllanos.meteo.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

/**
 * Permit to get the last current position of the user
 */
public class GpsManager {
    private static GpsManager instance;
    private LocationManager locationManager;
    private Activity activity;

    public static GpsManager getInstance() {
        if (instance == null) {
            instance = new GpsManager();
        }
        return instance;
    }

    public void init(Activity activity) {
        this.activity = activity;
        locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        // Request missing permission, do nothing on response because the permission are not mandatory for the application
        if (       ActivityCompat.checkSelfPermission(activity,  Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            String[] permissions = new String[]{ Manifest.permission.ACCESS_FINE_LOCATION , Manifest.permission.ACCESS_COARSE_LOCATION};
            ActivityCompat.requestPermissions(activity, permissions, 0 );
        }

    }

    /**
     * Retrieve the last know Location
     * @return
     */
    public Location getLastKnownLocation() {
        long timeGps = 0;
        long timeNet = 0;
        Location locationNet = null;
        Location locationGPS = null;
        if (ActivityCompat.checkSelfPermission(activity,  Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (null != locationGPS) {
                timeGps = locationGPS.getTime();
            }
        }

        if (ActivityCompat.checkSelfPermission(activity,  Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationNet = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (null != locationNet) {
                timeNet = locationNet.getTime();
            }
        }


        if ( timeNet < timeGps ) {
            return locationGPS;
        } else {
            return locationNet;
        }
    }
}
