package com.ingima.jllanos.meteo.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ingima.jllanos.meteo.R;
import com.ingima.jllanos.meteo.model.ItemCity;
import com.ingima.jllanos.meteo.utils.MeteoDataSource;
import com.ingima.jllanos.meteo.utils.SettingsHelper;

import java.util.Observable;
import java.util.Observer;

/**
 * ViewHolder of a ItemCity handle the fill of data from model using bind() function
 * This class observe the ItemCity model, to update display if model change
 */
public class ItemViewHolder extends AbstractViewHolder {
    private Context context;

    private TextView title;
    private ImageView icon;
    private TextView temp;
    private TextView weather;

    private TextView sunValue;
    private TextView tempValue;
    private TextView pressureValue;
    private TextView humidityValue;

    private ItemCity model;

    private final Observer observer = new Observer() {
        @Override
        public void update(Observable o, Object arg) {
            updateData();
        }
    };

    public ItemViewHolder(Context context, View view) {
        super(view);
        this.context = context;
        title     = (TextView) view.findViewById(R.id.title);
        icon      = (ImageView) view.findViewById(R.id.icon);
        temp      = (TextView) view.findViewById(R.id.temp);
        weather   = (TextView) view.findViewById(R.id.weather);
        sunValue  = (TextView) view.findViewById(R.id.sun_value);
        tempValue = (TextView) view.findViewById(R.id.temp_value);
        pressureValue = (TextView) view.findViewById(R.id.pressure_value);
        humidityValue = (TextView) view.findViewById(R.id.humidity_value);

        // Fill titles fields
        TextView sunTitle  = (TextView) view.findViewById(R.id.sun_title);
        TextView tempTitle = (TextView) view.findViewById(R.id.temp_title);
        TextView pressureTitle = (TextView) view.findViewById(R.id.pressure_title);
        TextView humidityTitle = (TextView) view.findViewById(R.id.humidity_title);
        sunTitle.setText(context.getString(R.string.title_sun));
        tempTitle.setText(context.getString(R.string.title_temp));
        pressureTitle.setText(context.getString(R.string.title_pressure));
        humidityTitle.setText(context.getString(R.string.title_humidity));

        // Add listener to add/remove model observer on attach/detach event
        view.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {
                if (model != null) {
                    model.addObserver(observer);
                }
            }

            @Override
            public void onViewDetachedFromWindow(View v) {
                if (model != null) {
                    model.deleteObserver(observer);
                }
            }
        });
    }

    /**
     * Bind new model to Item View
     * @param item
     */
    public void bind(ItemCity item) {
        this.model = item;
        updateData();
    }

    /**
     * Update data displayed
     */
    private void updateData() {

        title.setText(model.getName()+", "+model.getCountry());

        if (model.getTemp() != null) {
            temp.setText(String.format("%.2f %s", model.getTemp(), SettingsHelper.getInstance(context).getUnitsText()));
        } else {
            temp.setText("");
        }

        weather.setText(model.getWeatherDesc());
        Glide.with(context)
                .load(model.getWeatherIcon())
                .fitCenter()
                .into(icon);

        if (model.getSunset() != null) {
            sunValue.setText(String.format("%tH:%tM - %tH:%tM", model.getSunrise(), model.getSunrise() ,model.getSunset(),model.getSunset()));

        } else {
            sunValue.setText("");
        }

        if (model.getTempMin() != null) {
            tempValue.setText(String.format("%.2f - %.2f %s", model.getTempMin(), model.getTempMax(), SettingsHelper.getInstance(context).getUnitsText()));
        } else {
            tempValue.setText("");
        }

        if (model.getPressure() != null) {
            pressureValue.setText(String.format("%.0f %s", model.getPressure(), context.getString(R.string.units_pressure)));
        } else {
            pressureValue.setText("");
        }

        if (model.getHumidity() != null) {
            humidityValue.setText(String.format("%.2f %s", model.getHumidity(), context.getString(R.string.units_humidity)));
        } else {
            humidityValue.setText("");
        }
    }
}
