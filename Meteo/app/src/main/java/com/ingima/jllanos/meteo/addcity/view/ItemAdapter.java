package com.ingima.jllanos.meteo.addcity.view;

import android.content.Context;
import android.view.View;

import com.ingima.jllanos.meteo.R;
import com.ingima.jllanos.meteo.model.ItemCity;
import com.ingima.jllanos.meteo.view.AbstractItemAdapter;

import java.util.List;

/**
 * ArrayAdapter: inflate each item view and initialize his viewHolder
 */
public class ItemAdapter extends AbstractItemAdapter<ItemViewHolder> {

    public ItemAdapter(List<ItemCity> cityList) {
        super(cityList);
    }

    @Override
    public ItemViewHolder getNewViewHolder(Context context, View view) {
        return new ItemViewHolder(context, view);
    }


    @Override
    public int getLayoutRessource() {
        return R.layout.list_item_add_city;
    }



}
