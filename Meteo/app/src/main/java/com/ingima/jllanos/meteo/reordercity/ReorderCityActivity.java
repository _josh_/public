package com.ingima.jllanos.meteo.reordercity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.ingima.jllanos.meteo.R;
import com.ingima.jllanos.meteo.model.ItemCity;
import com.ingima.jllanos.meteo.reordercity.view.ItemAdapter;
import com.ingima.jllanos.meteo.utils.CityManager;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Screen used to re-order the list of Cities and permit to remove a city
 * observe CityManager to reload the list when change (delete/re-order)
 */
public class ReorderCityActivity extends AppCompatActivity {
    private RecyclerView listView;
    private CityManager cityManager;
    LinearLayoutManager layoutManager;

    private final Observer observer = new Observer() {
        @Override
        public void update(Observable o, Object arg) {
            reloadList();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reorder_city);
        listView = (RecyclerView) findViewById(R.id.list_item);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        listView.setLayoutManager(layoutManager);
    }

    @Override
    protected void onResume() {
        super.onResume();
        cityManager = CityManager.getInstance();
        reloadList();
        cityManager.addObserver(observer);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cityManager.deleteObserver(observer);
    }

    /**
     * Reload List stored by CityManager
     */
    private void reloadList() {
        // Retrieve previous position on list to reload on same position
        int prevPos = layoutManager.findFirstVisibleItemPosition();
        List<ItemCity> listCities = cityManager.getCityList();
        RecyclerView.Adapter adapter = new ItemAdapter(listCities);
        // Set the list to display
        listView.setHasFixedSize(true);
        listView.setAdapter(adapter);
        listView.scrollToPosition(prevPos);
    }
}
