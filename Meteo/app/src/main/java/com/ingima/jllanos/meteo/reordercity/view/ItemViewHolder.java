package com.ingima.jllanos.meteo.reordercity.view;

import android.content.Context;
import android.location.Location;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.view.View;
import android.widget.TextView;

import com.ingima.jllanos.meteo.R;
import com.ingima.jllanos.meteo.model.ItemCity;
import com.ingima.jllanos.meteo.utils.CityManager;
import com.ingima.jllanos.meteo.view.AbstractViewHolder;
import com.ingima.jllanos.meteo.view.IViewHolder;

/**
 * ViewHolder of a Item, handle the fill of data from model using bind() function
 */
public class ItemViewHolder extends AbstractViewHolder  {
    public TextView title;
    public TextView coord;
    public AppCompatButton buttonDelete;
    public AppCompatImageButton buttonUp;
    public AppCompatImageButton buttonDown;
    private ItemCity itemCity;

    public ItemViewHolder(Context context, View view) {
        super(view);
        title = (TextView) view.findViewById(R.id.title);
        coord = (TextView) view.findViewById(R.id.coord);
        buttonDelete = (AppCompatButton) view.findViewById(R.id.button_delete_id);
        buttonUp     = (AppCompatImageButton) view.findViewById(R.id.button_up_id);
        buttonDown   = (AppCompatImageButton) view.findViewById(R.id.button_down_id);
        // Set delete button action
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (itemCity != null) {
                    // Action
                    CityManager.getInstance().remove(itemCity);
                }
            }
        });

        // Set Up button action
        buttonUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (itemCity != null) {
                    // action
                    CityManager.getInstance().moveUp(itemCity);
                }
            }
        });

        // Set Down button action
        buttonDown.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (itemCity != null) {
                    // action
                    CityManager.getInstance().moveDown(itemCity);
                }
            }
        });
    }

    /**
     * Bind data model on View
     * @param item model
     */
    public void bind(final ItemCity item) {
        title.setText(item.getName()+", "+item.getCountry());
        Location location = item.getLocation();
        if (location != null) {
            coord.setText(item.getLocation().getLongitude()+", "+item.getLocation().getLatitude());
        } else {
            coord.setText("");
        }
        itemCity = item;

    }

}
