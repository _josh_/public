package com.ingima.jllanos.meteo.model;

import android.location.Location;

/**
 * Model for specific City get from GPS localisation
 */
public class CurrentLocation extends ItemCity {
    public CurrentLocation(String title, Location location) {
        super(title, "", "", 0);
        this.location = location;
    }

    @Override
    public void update(ItemCity updatedeItemCity) {
        // Get the name/country of this city
        name    = updatedeItemCity.getName();
        country = updatedeItemCity.getCountry();

        super.update(updatedeItemCity);
    }
}
