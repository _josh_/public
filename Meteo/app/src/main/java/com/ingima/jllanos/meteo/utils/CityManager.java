package com.ingima.jllanos.meteo.utils;

import com.ingima.jllanos.meteo.database.DBManager;
import com.ingima.jllanos.meteo.model.ItemCity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Observable;

/**
 * Manager of the list of Cities displayed by the application (except the city due to local GPS position)
 * The list is sorted using the city position attribute
 */
public class CityManager extends Observable {
    private static  CityManager instance;
    private final DBManager dbManager;
    private final List<ItemCity> cityList = new ArrayList<>();

    private Comparator comparatorPosition = new Comparator<ItemCity>() {
        @Override
        public int compare(ItemCity a, ItemCity b) {
            return a.getPosition() - b.getPosition();
        }
    };

    public static CityManager getInstance() {
        if (instance == null) {
            instance = new CityManager();
        }
        return instance;
    }

    CityManager() {
        // Initialize the list from the database on application start
        dbManager = DBManager.getInstance();// DbManager.getInstance();
//        List<City> cityListDb = dbManager.getAllCity();
//        for (City city : cityListDb) {
//            cityList.add(new ItemCity(city));
//        }
        cityList.addAll(dbManager.getAllCity());
        // Sort the list
        Collections.sort(cityList, comparatorPosition);
    }


    /**
     * Add a City in the list
     * @param itemCity
     * @return true if success, false if fails (city already present)
     */
    public boolean add(ItemCity itemCity) {
        if (cityList.contains(itemCity)) {
            return false;
        }

        if (cityList.isEmpty()) {
            // first city
            itemCity.setPosition(0);
        } else {
            // Set 'position' of new Item after the last of list
            itemCity.setPosition(cityList.get(cityList.size()-1).getPosition() + 1);
        }

        // Add on list and DB
        cityList.add(itemCity);
        dbManager.createCity(itemCity);

        // Notify list change
        fireNotification();
        return true;
    }

    /**
     * Remove a City in the list
     * @param itemCity
     */
    public void remove(ItemCity itemCity) {
        // Remove on list and DB
        cityList.remove(itemCity);
        dbManager.removeCity(itemCity);

        // Notify list change
        fireNotification();
    }

    /**
     * Move up the position of a City in the list
     * @param itemCity
     */
    public void moveUp(ItemCity itemCity) {
        int index = cityList.indexOf(itemCity);
        int currentPosition = itemCity.getPosition();
        if (index > 0) {
            ItemCity prevItemCity = cityList.get(index-1);
            // swap position of itemCity and prevItemCity
            itemCity.setPosition(prevItemCity.getPosition());
            prevItemCity.setPosition(currentPosition);

            // Store new position on DB
            dbManager.updateCity(prevItemCity);
            dbManager.updateCity(itemCity);

            // reorder the list
            Collections.sort(cityList, comparatorPosition);

            // Notify list change
            fireNotification();
        }
    }

    /**
     * Move down the position of a City in the list
     * @param itemCity
     */
    public void moveDown(ItemCity itemCity) {
        int index = cityList.indexOf(itemCity);
        int currentPosition = itemCity.getPosition();
        if (index < cityList.size()-1) {
            ItemCity nextItemCity = cityList.get(index+1);
            // swap position of itemCity and nextItemCity
            itemCity.setPosition(nextItemCity.getPosition());
            nextItemCity.setPosition(currentPosition);

            // Store new position on DB
            dbManager.updateCity(nextItemCity);
            dbManager.updateCity(itemCity);

            // reorder the list
            Collections.sort(cityList, comparatorPosition);

            // Notify list change
            fireNotification();
        }
    }


    public List<ItemCity> getCityList() {
        return cityList;
    }

    /**
     * notify list change to observers
     */
    private void fireNotification() {
        setChanged();
        notifyObservers();
    }

}
