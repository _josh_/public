package com.ingima.jllanos.meteo.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Moi on 23/10/2017.
 */

abstract public class AbstractViewHolder extends RecyclerView.ViewHolder implements IViewHolder {

    public AbstractViewHolder(View itemView) {
        super(itemView);
    }
}
