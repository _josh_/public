package com.ingima.jllanos.meteo.utils;

import android.content.Context;
import android.location.Location;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.ingima.jllanos.meteo.R;
import com.ingima.jllanos.meteo.model.ItemCity;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;


import java.util.ArrayList;
import java.util.List;

/**
 * Handle all request to get Meteo Data provider (openweathermap)
 */
public class MeteoDataSource {
    private static MeteoDataSource instance;
    public static final String URL_ICONS = "http://openweathermap.org/img/w/";
    public static final String URL_ICONS_EXT = ".png";

    private static final String REQUEST_SEARCH   = "http://api.openweathermap.org/data/2.5/find?q=";
    private static final String REQUEST_UPDATE   = "http://api.openweathermap.org/data/2.5/group?id=";

    private static final String ATTR_UNITS       = "&units=";
    private static final String ATTR_API         = "&appid=";
    private static final String ATTR_LANG        = "&lang=";

    private static final String REQUEST_LOCALISATION = "http://api.openweathermap.org/data/2.5/weather?";
    private static final String ATTR_LAT = "lat=";
    private static final String ATTR_LON = "&lon=";


    static final String ATTR_METRIC      = "metric";
    static final String ATTR_FAHRENHEITH = "imperial";

    private String unitsAttrStr = "";
    private String langText = "en";

    public static MeteoDataSource getInstance() {
        if (instance == null) {
            instance = new MeteoDataSource();
        }
        return instance;
    }

    /**
     * Search list of cities from a input cityName
     * @param cityName
     * @param context
     * @return Future<JsonObject>
     */
    public Future<JsonObject> search(final String cityName, final Context context) {
        return doRequest(REQUEST_SEARCH+cityName,context);
    }

    /**
     * Will do a request using the itemCity.location to update Data on this city
     * @param context
     * @param itemCity city to update
     */
    public void updateFromLocalisation(final Context context, final ItemCity itemCity) {
        Location location = itemCity.getLocation();
        if (location != null) {
            String param = String.format("%s%f%s%f", ATTR_LAT, location.getLatitude(), ATTR_LON, location.getLongitude());
            doRequest(REQUEST_LOCALISATION + param, context).then(
                    new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if (e != null) {
                                e.printStackTrace();
                                return;
                            }
                            try {
                                // Extract data from result
                                ItemCity itemCityUpdated = new ItemCity(result);

                                // update original list
                                itemCity.update(itemCityUpdated);

                            } catch (Exception exception) {
                                exception.printStackTrace();
                            }
                        }
                    }
            );
        }
    }

    /**
     * Will do a request using the itemCity.id of each itemCity, to update Data on this itemCity model
     * @param context
     * @param listCities list of cities to update
     */
    public void update(final Context context, final List<ItemCity> listCities) {
        String idList = "";
        String id;

        // compute the list of id
        for (ItemCity itemCity : listCities) {
            id = itemCity.getCityId();
            if (!id.isEmpty()) {
                idList += id + ",";
            }
        }

        if (!idList.isEmpty()) {
            doRequest(REQUEST_UPDATE + idList.substring(0,idList.length()-1), context).then(
                    new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if (e != null) {
                                e.printStackTrace();
                                return;
                            }
                            try {
                                // Extract data from result
                                JsonArray jsonArr = (JsonArray) result.get("list");
                                List<ItemCity> listCitiesTmp = new ArrayList<>();
                                for (JsonElement elt : jsonArr) {
                                    listCitiesTmp.add(new ItemCity((JsonObject) elt));
                                }

                                // update original list
                                int index;
                                for (ItemCity itemCityUpdated : listCitiesTmp) {
                                    index = listCities.indexOf(itemCityUpdated);
                                    if (index >= 0) {
                                        listCities.get(index).update(itemCityUpdated);
                                    }
                                }

                            } catch (Exception exception) {
                                exception.printStackTrace();
                            }
                        }
                    }
            );
        }
    }


    /**
     * Do a request, adding API, Lang, Unit parameters
     * @param url
     * @param context
     * @return Future<JsonObject>
     */
    private Future<JsonObject> doRequest(final String url, final Context context) {
        return Ion.with(context)
                .load(url+ATTR_API+context.getString(R.string.api_id) +  unitsAttrStr + ATTR_LANG + langText)
                .asJsonObject();
    }

    /**
     * Extract from a JsonObject Response the list of ItemCity
     * @param json JsonObject
     * @return list of ItemCity
     */
    public List<ItemCity> getCityListFromJsonResponse(JsonObject json) {
        List<ItemCity> cityList = new ArrayList<>();
        try {
            JsonArray jsonArr = (JsonArray) json.get("list");
            for (JsonElement elt : jsonArr) {
                cityList.add(new ItemCity((JsonObject) elt));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cityList;
    }



    public void setLangText(String langText) {
        this.langText = langText;
    }

    /**
     * Set the url paramter used for temperature units
     * @param unitsTemp
     */
    public void setUnitsTemp(String unitsTemp) {
        switch (unitsTemp) {
            case SettingsHelper.UNIT_METRIC_NAME:
                unitsAttrStr = ATTR_UNITS + ATTR_METRIC;
                break;
            case SettingsHelper.UNIT_FAHRENHEITH_NAME:
                unitsAttrStr = ATTR_UNITS + ATTR_FAHRENHEITH;
                break;
            default:
                unitsAttrStr = "";
        }
    }
}
