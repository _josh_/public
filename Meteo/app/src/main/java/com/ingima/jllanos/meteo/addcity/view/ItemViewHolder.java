package com.ingima.jllanos.meteo.addcity.view;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ingima.jllanos.meteo.R;
import com.ingima.jllanos.meteo.model.ItemCity;
import com.ingima.jllanos.meteo.utils.CityManager;
import com.ingima.jllanos.meteo.view.AbstractViewHolder;
import com.ingima.jllanos.meteo.view.IViewHolder;

/**
 * ViewHolder of a Item, handle the fill of data from model using bind() function
 */
public class ItemViewHolder extends AbstractViewHolder {
    public TextView title;
    public TextView coord;
    public AppCompatButton buttonAdd;
    ItemCity itemCity;

    public ItemViewHolder(final Context context, View view) {
        super(view);
        title = (TextView) view.findViewById(R.id.title);
        coord = (TextView) view.findViewById(R.id.coord);
        buttonAdd = (AppCompatButton) view.findViewById(R.id.button_id);
        // Set Add button action
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (itemCity != null) {
                    // Add the item action
                    if (CityManager.getInstance().add(itemCity)) {
                        // sucess -> back on previous screen
                        ((Activity) context).finish();
                    } else {
                        // Fail display message
                        Toast toast = Toast.makeText(context, String.format(context.getString(R.string.error_fail_add), itemCity.getName()), Toast.LENGTH_SHORT);
                        toast.show();
                    }
                }
            }
        });
    }


    /**
     * Bind data model on View
     * @param item model
     */
    public void bind(final ItemCity item) {
        title.setText(item.getName()+", "+item.getCountry());
        coord.setText(item.getLocation().getLongitude()+", "+item.getLocation().getLatitude());
        itemCity = item;
    }

}
