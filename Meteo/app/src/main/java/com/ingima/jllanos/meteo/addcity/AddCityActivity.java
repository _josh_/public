package com.ingima.jllanos.meteo.addcity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ingima.jllanos.meteo.R;
import com.ingima.jllanos.meteo.addcity.view.ItemAdapter;
import com.ingima.jllanos.meteo.model.ItemCity;
import com.ingima.jllanos.meteo.utils.MeteoDataSource;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

/**
 * Screen to add a cities
 * display a Search input (with keyboard search), Search/Cancel buttons and list of search result.
 */
public class AddCityActivity extends AppCompatActivity {
    private RecyclerView listView;
    RecyclerView.LayoutManager layoutManager;
    private EditText searchText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_city);

        listView = (RecyclerView) findViewById(R.id.list_item);
        searchText = (EditText) findViewById(R.id.search_text);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        listView.setLayoutManager(layoutManager);

        // Button Search configuration
        Button buttonSearch = (Button)  findViewById(R.id.search_button);
        buttonSearch.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                performSearch();
            }
        });

        // Button cancel configuration
        Button buttonCancel = (Button)  findViewById(R.id.search_cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });

        // Keyboard Search action configuration
        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();
                    return true;
                }
                return false;
            }
        });
    }

    /**
     * Do search and update the result list
     */
    private void performSearch() {
        try {
            String cityName = searchText.getText().toString();
            MeteoDataSource.getInstance().search(cityName, this).then(
                    new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            // on request result, update the list
                            if (e != null) {
                                Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.error_search), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            reloadList(MeteoDataSource.getInstance().getCityListFromJsonResponse(result));
                        }
                    }
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Reload the result list
     * @param listCities Array of cities to display
     */
    private void reloadList(List<ItemCity> listCities) {
        RecyclerView.Adapter adapter = new ItemAdapter(listCities);

        // Set the list to display
        listView.setHasFixedSize(true);
        listView.setAdapter(adapter);
    }
}
