package com.ingima.jllanos.meteo.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.ingima.jllanos.meteo.R;
import com.ingima.jllanos.meteo.model.ItemCity;

import java.util.List;


abstract public class AbstractItemAdapter<T extends AbstractViewHolder> extends RecyclerView.Adapter<T> {

    List<ItemCity> model;
    public AbstractItemAdapter(List<ItemCity> cityList) {
        model = cityList;
    }

    @Override
    public T onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(getLayoutRessource(), parent, false);
        T viewHolder =  getNewViewHolder(context, view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(T holder, int position) {
        holder.bind(model.get(position));
    }

    @Override
    public int getItemCount() {
        return model.size();
    }

    public abstract T getNewViewHolder(Context context, View view);
    public abstract int getLayoutRessource();
}
