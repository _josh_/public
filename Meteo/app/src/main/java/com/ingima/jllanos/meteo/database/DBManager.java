package com.ingima.jllanos.meteo.database;

import android.content.Context;

import com.ingima.jllanos.meteo.model.ItemCity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * Manage the DataBase
 */
public class DBManager {

    private DatabaseHelper  helper;
    private static DBManager instance;

    public static void init(Context context) {
        if (instance == null)
            instance = new DBManager(context);
    }

    public static DBManager getInstance() {
        return instance;
    }

    private DBManager(Context context) {
        helper = new DatabaseHelper(context);
    }

    private DatabaseHelper getHelper() {
        return helper;
    }

    /** Methods [Person] **/

    public List<ItemCity> getAllCity() {
        try {
            return getHelper().getCityDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public long createCity(ItemCity city) {
        try {
            getHelper().getCityDao().create(city);
            return city.getId();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public void removeCity(ItemCity city) {
        try {
            getHelper().getCityDao().delete(city);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public long updateCity(ItemCity city) {
        try {
            getHelper().getCityDao().update(city);
            return city.getId();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }
}